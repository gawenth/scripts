# Scripts
Scripts para la integración continua en un Centos7 con Spring, Git y Dockers

### Uso
```
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/test.sh | sh -s -- param1 param2
```
### Centos
Instalar Centos7 con todas las herramientas
```
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/centos.sh | cat
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/centos.sh | sh
```
### Spring
Configurar sistema de carpetas, git, mysql y mongo
```
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/spring.sh | cat
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/spring.sh | sh -s -- <project> <profile> <sql_pass> <mongo_pass>
```
### Remove Spring
Eliminar sistema de carpetas, git, mysql y mongo
```
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/remove.sh | cat
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/remove.sh | sh -s -- <project>
```
### Ftp
Crear un ftp
```
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/ftp.sh | cat
curl -fsSL https://bitbucket.org/gawenth/scripts/downloads/ftp.sh | sh -s -- <host> <username> <password> <quota>
```


